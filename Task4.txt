using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task4 : MonoBehaviour
{
    
    void Start()
    {
            int people = 1000;
            int floor = 1;

            for (floor = 1; floor < 34; floor++)
            {
                if (floor % 2 != 0)
                {
                    people = people - people / 10;
                }
                else
                {
                    people = people - 66;
                }

                if (people <= 0)
                {
                    Debug.Log(floor - 1);
                    break;
                }
            }
        
    }
}